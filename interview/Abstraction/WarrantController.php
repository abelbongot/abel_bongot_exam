<?php

namespace Interview\Abstraction;

use Interview\Abstraction\Request;

class WarrantController implements Request
{

    /**
     * Returns a response object with a successful result
     *
     * Create a new class (PSR-4 auto loading is already configured in composer) that implements the
     * Response interface, and return an instance of that class. It should produce a 200 response
     * code, and provides a JSON encoded message that should have the following structure:
     *
     * ```json
     * {"success": true, "message": "Warrant created for <individual name>", "issuedBy": "<judge name>"}
     * ```
     *
     * Both the individual and judge names should be in the format "<last name>, <first name>".
     * This method receives a request object containing a json payload with the following structure:
     *
     * ```json
     * {
     *  "Event": {
     *     "EventType": "Warrant",
     *     "Individual": {
     *       "IndividualId": <number>,
     *       "FirstName": <name>,
     *       "LastName": <name>
     *     },
     *     "Judge": {
     *       "JudgeId": <number>,
     *       "FirstName": <name>,
     *       "LastName": <name>
     *     },
     *     "Court": {
     *       "Address": <address>
     *     },
     *     "Offenses": [
     *        {"Code": <code>},
     *        ...
     *     ]
     *   }
     * }
     * ```
     *
     * This should also validate that we have an IndividualId, JudgeId, and at least one offense. If it
     * is missing any of these, we should instead get a 422 response code. Also, if the method of the
     * request is not a POST, we want to send a 405 response code.
     *
     * @param Request $request
     * @return Response
     */
    public function createWarrant(Request $request)
    {
        $this->request = $request;
        $method = $this->getMethod();
        $payload = $this->getPayload();

        $response = new Response;
        $response->method = $method;
        $response->payload = $payload;
        $response->init();
        return $response;     

    }
    public function getMethod()
    {
        return $this->request->method;
    }
    public function getPayload()
    {
        return json_decode($this->request->payload);
    }
}