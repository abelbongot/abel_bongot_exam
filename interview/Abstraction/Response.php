<?php

namespace Interview\Abstraction;
class Response implements ResponseInterface
{
    public $method;
    public $payload;
    public $response;
    public $statusCode;
    public $success;
    public $message;
    public $issuedBy;
    public function init()
    {
        $this->success = 'false';
        $this->message = 'Warrant created for';
        $this->message = $this->getMessage();
        
        $this->issuedBy = $this->getIssuedBy();
        if( !$this->WrongMethod() ){
            $this->response = $this->getResponse();
            $this->success = 'true';
            if ($this->MissingIndividualId() || $this->MissingJudgeId() || $this->MissingOffenses()) {
                $this->statusCode = 422;
            } else {
                $this->statusCode = 200;
            }
        }else{
            $this->statusCode = 405;
        }
        $this->statusCode = $this->getStatusCode();
    }
    public function getBody()
    {
        return json_encode(array(
            'success' => $this->success,
            'message' => $this->message,
            'issuedBy' => $this->issuedBy
        ));
    }
    public function WrongMethod()
    {
        if ($this->method === 'POST') {
            return true;
        }else{
            return false;
        }
    }
    public function MissingOffenses()
    {
        $countOffenses = (isset($this->payload->Event->Offenses) && count($this->payload->Event->Offenses)) ? count($this->payload->Event->Offenses) : TRUE;
        return $countOffenses;
    }
    public function MissingIndividualId()
    {
        $IndividualId  = (isset($this->payload->Event->Individual->IndividualId) && $this->payload->Event->Individual->IndividualId != '') ? $this->payload->Event->Individual->IndividualId : TRUE;
        return $IndividualId;
    }
    public function MissingJudgeId()
    {
        $JudgeId       = (isset($this->payload->Event->Judge->JudgeId) && $this->payload->Event->Judge->JudgeId != '' ) ? $this->payload->Event->Judge->JudgeId : TRUE;
        return $JudgeId;
    }
    public function getResponse()
    {
        return json_encode('{"success": '.$this->success.', "message": "'. $this->getMessage() . ', "issuedBy": "' . $this->getIssuedBy() . '"}');

    }
    public function getMessage()
    {
        $IndividualFirstName  = isset($this->payload->Event->Individual->FirstName) ? $this->payload->Event->Individual->FirstName : '';
        $IndividualLastName  = isset($this->payload->Event->Individual->LastName) ? $this->payload->Event->Individual->LastName : '';
        return $this->message . ' ' . $IndividualLastName . ', ' . $IndividualFirstName;
    }
    public function getIssuedBy()
    {
        $JudgeFirstName       = isset($this->payload->Event->Judge->FirstName) ? $this->payload->Event->Judge->FirstName : '';
        $JudgeLastName       = isset($this->payload->Event->Judge->LastName) ? $this->payload->Event->Judge->LastName : '';
        return $JudgeLastName . ', ' . $JudgeFirstName;
    }
    public function getStatusCode()
    {
        return $this->statusCode;
    }

}