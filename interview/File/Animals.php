<?php

namespace Interview\File;

class Animals
{

    var $dlm_row = 'DLM_ROW';
    var $dlm_fld = ',';
    /**
     * Returns the name of the fastest animal from the animals.txt in this directory
     *
     * @return string
     */
    public function getFastestAnimalName()
    {
        $data = $this->readFile();
        $arr_rows = explode($this->dlm_row, $data);
        $ctr = 1;
        $fastest_speed = 0;
        $fastest_animal = '';

        foreach ($arr_rows as $animal) {
            if ($ctr > 1 && $animal != '') {
                $animal_data = explode($this->dlm_fld, $animal);
                $cur_animal_speed = $animal_data[2];//with mph or km/h txt
                if( stristr($cur_animal_speed,'mph') != FALSE ){ //if mph
                    $cur_speed = str_replace('mph', '', $cur_animal_speed);//get the number only without text
                }else{ //km/h
                    $cur_speed = $this->convert_kph_to_mph( str_replace('km/h', '', $cur_animal_speed) );
                }
                if( $cur_speed > $fastest_speed ){
                    $fastest_speed = $cur_speed;
                    $fastest_animal = $animal_data[0];
                }
            }
            $ctr++;
        }
        return $fastest_animal;
    }

    private function convert_kph_to_mph( $kph )
    {
        return ($kph * 0.621371);
    }

    private function convert_mph_to_kmh($mph)
    {
        return ($mph * 1.609);
    }

    /**
     * Get the slowest animals speed in km/h
     *
     * We're OK with heuristics, we can multiply MPH by 1.609 to get km/h
     *
     * @return float
     */
    public function getSlowestAnimalSpeed()
    {
        $data = $this->readFile();
        $arr_rows = explode($this->dlm_row, $data);
        $ctr = 1;
        $slowest_speed = 9999;
        // $slowest_animal = '';

        foreach ($arr_rows as $animal) {
            if ($ctr > 1 && $animal != '') {
                $animal_data = explode($this->dlm_fld, $animal);
                $cur_animal_speed = $animal_data[2]; //with mph or km/h txt
                if (stristr($cur_animal_speed, 'mph') != FALSE) { //if mph
                    $cur_speed = $this->convert_mph_to_kmh( str_replace('mph', '', $cur_animal_speed) ); //get the number only without text
                } else { //km/h
                    $cur_speed = str_replace('km/h', '', $cur_animal_speed);
                }
                if ($cur_speed < $slowest_speed) {
                    $slowest_speed = $cur_speed;
                    // $slowest_animal = $animal_data[0];
                }
            }
            $ctr++;
        }
        return $slowest_speed;
    }

    /**
     * Produces an array where each key is the animal name, and each value is the number of votes
     *
     * @return array
     */
    public function getVotesByAnimal()
    {
        $data = $this->readFile();
        $arr_rows = explode($this->dlm_row, $data);
        $ctr = 1;
        $slowest_speed = 9999;
        // $slowest_animal = '';
        $arr_votes = array();
        foreach ($arr_rows as $animal) {
            if ($ctr > 1 && $animal != '') {
                $animal_data = explode($this->dlm_fld, $animal);
                $cur_animal_speed = $animal_data[2]; //with mph or km/h txt
                if (stristr($cur_animal_speed, 'mph') != FALSE) { //if mph
                    $cur_speed = $this->convert_mph_to_kmh(str_replace('mph', '', $cur_animal_speed)); //get the number only without text
                } else { //km/h
                    $cur_speed = str_replace('km/h', '', $cur_animal_speed);
                }
                if ($cur_speed < $slowest_speed) {
                    $slowest_speed = $cur_speed;
                    // $slowest_animal = $animal_data[0];
                }
                $animal = $animal_data[0];
                $votes = $animal_data[3];
                $arr_votes[ $animal ] = str_replace("\n","",$votes);
            }
            $ctr++;
        }
        return $arr_votes;
    }

    private function readFile(){
        $myfile = fopen(dirname(__FILE__) . "\animals.txt", "r") or die("Unable to open file!");
        // Output one line until end-of-file
        $str='';
        while (!feof($myfile)) {
            $str .= fgets($myfile) . $this->dlm_row;
        }
        fclose($myfile);
        return $str;
    }

}